""" Cython module containing a dynamic and static is_prime implementation """

cdef extern from "math.h":
    double sqrt(double m)
    double floor(double x)
    double fmod(double x, double y)

import math

def is_prime(n):
    # Dynamic
    if n == 0 or n == 1 or n > 2 and n % 2 == 0:
        return False

    sqrt_n = math.floor(math.sqrt(n))

    for i in range(3, sqrt_n + 1, 2):
        if n % i == 0:
            return False
    return True

def is_primeC(double n):
    # Static
    if n == 0.0 or n == 1.0 or n > 2.0 and fmod(n, 2.0) == 0.0:
        return False

    sqrt_n = floor(sqrt(n))

    cdef double i = 3.0
    while i < sqrt_n + 1.0:
        if fmod(n, i) == 0.0:
            return False
        i = i + 2.0
    return True