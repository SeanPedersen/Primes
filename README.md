## Crunching Primes
### Using Python & Cython
**Compiling the Cython module:**
> cd primes/

> python setup.py build_ext --inplace

**Word list files**
- https://github.com/first20hours/google-10000-english

**Run Jupyter Notebook**
> jupyter notebook